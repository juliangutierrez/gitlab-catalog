class CreateCatalogLicenses < ActiveRecord::Migration
  def change
    create_table :catalog_licenses do |t|
      t.string :name
      t.string :short_name
      t.string :url

      t.timestamps null: false
    end
  end
end
