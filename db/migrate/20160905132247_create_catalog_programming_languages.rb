class CreateCatalogProgrammingLanguages < ActiveRecord::Migration
  def change
    create_table :catalog_programming_languages do |t|
      t.string :name
      t.string :url

      t.timestamps null: false
    end
  end
end
