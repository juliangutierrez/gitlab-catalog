# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class AddFieldsToCatalogProjects < ActiveRecord::Migration
  include Gitlab::Database::MigrationHelpers

  # Set this constant to true if this migration requires downtime.
  DOWNTIME = false

  # When a migration requires downtime you **must** uncomment the following
  # constant and define a short and easy to understand explanation as to why the
  # migration requires downtime.
  # DOWNTIME_REASON = ''

  # When using the methods "add_concurrent_index" or "add_column_with_default"
  # you must disable the use of transactions as these methods can not run in an
  # existing transaction. When using "add_concurrent_index" make sure that this
  # method is the _only_ method called in the migration, any other changes
  # should go in a separate migration. This ensures that upon failure _only_ the
  # index creation fails and can be retried or reverted easily.
  #
  # To disable transactions uncomment the following line and remove these
  # comments:
  # disable_ddl_transaction!

  def change
    add_column :catalog_projects, :visibility, :integer
    add_column :catalog_projects, :has_country_property, :boolean
    add_column :catalog_projects, :operational_systems, :text
    add_column :catalog_projects, :frameworks, :text
    add_column :catalog_projects, :dependencies, :text
    add_column :catalog_projects, :using_since, :string
    add_column :catalog_projects, :has_services_by_companies, :boolean
    add_column :catalog_projects, :documentation_status, :integer
    add_column :catalog_projects, :documentation_link, :string
    add_column :catalog_projects, :is_used_by_companies, :boolean
    add_column :catalog_projects, :plataform, :integer
  end
end
