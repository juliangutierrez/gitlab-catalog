class CreateCatalogProjects < ActiveRecord::Migration
  def change
    create_table :catalog_projects do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.string :url

      t.timestamps null: false
    end

    create_table :catalog_countries_developing_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end

     create_table :catalog_countries_using_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end

     create_table :catalog_countries_interested_in_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end

    create_table :catalog_programming_languages_projects, id: false do |t|
      t.belongs_to :project, index: true
      t.belongs_to :programming_language, index: { name: 'index_catalog_planguages_projects_on_planguages_id' }
    end

    create_table :catalog_licenses_projects, id: false do |t|
      t.belongs_to :project, index: true
      t.belongs_to :license, index: true
    end

    create_table :catalog_categories_projects, id: false do |t|
      t.belongs_to :project, index: true
      t.belongs_to :category, index: true
    end
  end
end
