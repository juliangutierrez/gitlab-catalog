class CreateCatalogCountries < ActiveRecord::Migration
  def change
    create_table :catalog_countries do |t|
      t.string :name
      t.string :slug
      t.string :flag

      t.timestamps null: false
    end
  end
end
