require 'test_helper'

class Catalog::LicensesControllerTest < ActionController::TestCase
  setup do
    @catalog_license = catalog_licenses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catalog_licenses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catalog_license" do
    assert_difference('Catalog::License.count') do
      post :create, catalog_license: { name: @catalog_license.name, short_name: @catalog_license.short_name, url: @catalog_license.url }
    end

    assert_redirected_to catalog_license_path(assigns(:catalog_license))
  end

  test "should show catalog_license" do
    get :show, id: @catalog_license
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catalog_license
    assert_response :success
  end

  test "should update catalog_license" do
    patch :update, id: @catalog_license, catalog_license: { name: @catalog_license.name, short_name: @catalog_license.short_name, url: @catalog_license.url }
    assert_redirected_to catalog_license_path(assigns(:catalog_license))
  end

  test "should destroy catalog_license" do
    assert_difference('Catalog::License.count', -1) do
      delete :destroy, id: @catalog_license
    end

    assert_redirected_to catalog_licenses_path
  end
end
