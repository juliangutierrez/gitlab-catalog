require 'test_helper'

class Catalog::ProjectsControllerTest < ActionController::TestCase
  setup do
    @catalog_project = catalog_projects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catalog_projects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catalog_project" do
    assert_difference('Catalog::Project.count') do
      post :create, catalog_project: { description: @catalog_project.description, name: @catalog_project.name, slug: @catalog_project.slug, url: @catalog_project.url }
    end

    assert_redirected_to catalog_project_path(assigns(:catalog_project))
  end

  test "should show catalog_project" do
    get :show, id: @catalog_project
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catalog_project
    assert_response :success
  end

  test "should update catalog_project" do
    patch :update, id: @catalog_project, catalog_project: { description: @catalog_project.description, name: @catalog_project.name, slug: @catalog_project.slug, url: @catalog_project.url }
    assert_redirected_to catalog_project_path(assigns(:catalog_project))
  end

  test "should destroy catalog_project" do
    assert_difference('Catalog::Project.count', -1) do
      delete :destroy, id: @catalog_project
    end

    assert_redirected_to catalog_projects_path
  end
end
