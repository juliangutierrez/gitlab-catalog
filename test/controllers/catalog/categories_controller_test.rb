require 'test_helper'

class Catalog::CategoriesControllerTest < ActionController::TestCase
  setup do
    @catalog_category = catalog_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catalog_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catalog_category" do
    assert_difference('Catalog::Category.count') do
      post :create, catalog_category: { name: @catalog_category.name }
    end

    assert_redirected_to catalog_category_path(assigns(:catalog_category))
  end

  test "should show catalog_category" do
    get :show, id: @catalog_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catalog_category
    assert_response :success
  end

  test "should update catalog_category" do
    patch :update, id: @catalog_category, catalog_category: { name: @catalog_category.name }
    assert_redirected_to catalog_category_path(assigns(:catalog_category))
  end

  test "should destroy catalog_category" do
    assert_difference('Catalog::Category.count', -1) do
      delete :destroy, id: @catalog_category
    end

    assert_redirected_to catalog_categories_path
  end
end
