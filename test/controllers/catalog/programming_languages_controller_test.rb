require 'test_helper'

class Catalog::ProgrammingLanguagesControllerTest < ActionController::TestCase
  setup do
    @catalog_programming_language = catalog_programming_languages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catalog_programming_languages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catalog_programming_language" do
    assert_difference('Catalog::ProgrammingLanguage.count') do
      post :create, catalog_programming_language: { name: @catalog_programming_language.name, url: @catalog_programming_language.url }
    end

    assert_redirected_to catalog_programming_language_path(assigns(:catalog_programming_language))
  end

  test "should show catalog_programming_language" do
    get :show, id: @catalog_programming_language
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catalog_programming_language
    assert_response :success
  end

  test "should update catalog_programming_language" do
    patch :update, id: @catalog_programming_language, catalog_programming_language: { name: @catalog_programming_language.name, url: @catalog_programming_language.url }
    assert_redirected_to catalog_programming_language_path(assigns(:catalog_programming_language))
  end

  test "should destroy catalog_programming_language" do
    assert_difference('Catalog::ProgrammingLanguage.count', -1) do
      delete :destroy, id: @catalog_programming_language
    end

    assert_redirected_to catalog_programming_languages_path
  end
end
