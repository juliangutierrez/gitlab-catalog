require 'test_helper'

class Catalog::CountriesControllerTest < ActionController::TestCase
  setup do
    @catalog_country = catalog_countries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catalog_countries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catalog_country" do
    assert_difference('Catalog::Country.count') do
      post :create, catalog_country: { flag: @catalog_country.flag, name: @catalog_country.name, slug: @catalog_country.slug }
    end

    assert_redirected_to catalog_country_path(assigns(:catalog_country))
  end

  test "should show catalog_country" do
    get :show, id: @catalog_country
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catalog_country
    assert_response :success
  end

  test "should update catalog_country" do
    patch :update, id: @catalog_country, catalog_country: { flag: @catalog_country.flag, name: @catalog_country.name, slug: @catalog_country.slug }
    assert_redirected_to catalog_country_path(assigns(:catalog_country))
  end

  test "should destroy catalog_country" do
    assert_difference('Catalog::Country.count', -1) do
      delete :destroy, id: @catalog_country
    end

    assert_redirected_to catalog_countries_path
  end
end
