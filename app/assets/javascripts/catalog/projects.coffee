# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	$('#query').on 'focusout', (event) =>
		$('.search-catalog-icon').removeClass 'focus' ;

	$('#query').on 'click', (event) =>
		$('.search-catalog-icon').addClass 'focus' ;