$(function() {
  $('.catalog-datepicker').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'mm/yy',
    beforeShow: function() {
    	$('.catalog-datepicker').datepicker('widget').addClass('hide-calendar');
    },
    onClose: function(dateText, inst) { 
      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
      $(this).datepicker('setDate', new Date(year, month, 1));
    }
  });
});

var catalog = window.catalog = {};

catalog.Project = function(formGroup) {
	this._ids = [];
	this._inputs = $(formGroup + ' ul input')
	this._input = $(formGroup + ' .field');
	this._attribute = $(formGroup).data('name');
	this._nestedAttribute = $(formGroup).data('nested-name');
	this._url = $(formGroup).data('url');
	this._dropdown = formGroup + ' .results';
	this._addButton = $(formGroup + ' button.btn-primary');
	this._idSelected = $(formGroup + ' #selected_id');
	this._list = formGroup + ' ul.list-inline';
	this._nested = $(formGroup).data('nested');
  this._init();
};

catalog.Project.prototype = {
	
	_init: function() {
		var self = this;

		this._inputs.each(function() {
			self._ids.push($(this).val()); 
		});

		this._initAutocomplete();
		this._initAdd();
		this._initRemove();
		if (this._nested) this._initInput();
	},

	_initAutocomplete: function() {
	  this._input
	    .autocomplete({
	      source: this._url,
	      appendTo: this._dropdown,
	      select: $.proxy(this._select, this)
	    })
	    .autocomplete('instance')._renderItem = $.proxy(this._render, this);
	},

	_initInput: function() {
		var self = this;

		this._input.on('keyup', function() {
			if (self._input.val().trim() == '') {
				self._addButton.attr("disabled","disabled");
			} else {
				self._addButton.removeAttr("disabled");
			};
		});
	},

	_initAdd: function() {
		var self = this;

		this._addButton.on('click', function() {
			var idSelected = self._idSelected.val();
			if (!(self._ids.includes(idSelected))) {
					$(self._list).append("<li class='label label-primary'>
																									<span class='pull-left'>" + 
																									self._input.val() +	"</span>" + 
																									"<button type='button' class='close' aria-label='Close'>
																									<span aria-hidden='true'>&times;</span></button></li>" );
					if (self._nested && idSelected === '') {
						var nestedAttribute = self._nestedAttribute.replace('INDEX', $.now());
						$(self._list + ' li').last().append("<input name=" + nestedAttribute + " value=" + self._input.val() + " type='hidden'>");
					} else {
						$(self._list + ' li').last().append("<input name=" + self._attribute +" value=" + idSelected + " type='hidden'>");
						self._ids.push(idSelected);
						self._idSelected.val('');
					};
			};

			self._input.val('');
			self._addButton.attr("disabled","disabled").blur();
			self._initRemove();
		})
	},

	_initRemove: function() {
		var self = this;

		$('button.close').on('click', function(){
			self._ids = _.without(self._ids, $(this).siblings('input').val());
			$(this).parents('li').remove();
		});
	},

	_select: function(e, ui) {
	  this._input.val(ui.item.name);
	  this._idSelected.val(ui.item.id);
	  this._addButton.removeAttr("disabled");
	  return false;
	},

	_render: function(ul, item) {
	  return $('<li>')
	    .append(item.name)
	    .appendTo(ul);
	}

};