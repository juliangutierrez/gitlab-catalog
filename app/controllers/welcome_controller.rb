class WelcomeController < ApplicationController
	def index
		@countries_projects_count = User.can_create_projects_with_public_count.last(5).reverse
	end

	def show_member
		@country = User.find_by_username(params[:id])
	end

	def change_locale
		session[:locale] = params[:locale]
		I18n.locale = session[:locale]
		redirect_to request.referer
	end
end