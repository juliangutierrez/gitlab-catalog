class Catalog::CategoriesController < ApplicationController
  before_action :set_catalog_category, only: [:show, :edit, :update, :destroy, :projects]
  before_action :verify_authentication, except: [:index, :projects]

  # GET /catalog/categories
  def index
    @catalog_categories = Catalog::Category.all.order(:name)
  end
  
  # GET /catalog/categories/new
  def new
    @catalog_category = Catalog::Category.new
  end

  # GET /catalog/categories/1/edit
  def edit
  end

  # POST /catalog/categories
  def create
    @catalog_category = Catalog::Category.new(catalog_category_params)

    if @catalog_category.save
      redirect_to @catalog_category, notice: 'Category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /catalog/categories/1
  def update
    if @catalog_category.update(catalog_category_params)
      redirect_to @catalog_category, notice: 'Category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /catalog/categories/1
  def destroy
    @catalog_category.destroy
    redirect_to catalog_categories_url, notice: 'Category was successfully destroyed.'
  end

  def projects
    @catalog_projects = @catalog_category.projects.search(params[:query])
    @query = params[:query]
    render 'catalog/projects/index' 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog_category
      @catalog_category = Catalog::Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_category_params
      params.require(:catalog_category).permit(:name)
    end

    def verify_authentication
      head(403) and return unless current_user.present? && current_user.is_admin?
    end
end
