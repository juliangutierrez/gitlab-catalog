class Catalog::ProgrammingLanguagesController < ApplicationController
  before_action :set_catalog_programming_language, only: [:show, :edit, :update, :destroy]
  before_action :verify_authentication

  # GET /catalog/programming_languages
  def index
    @catalog_programming_languages = Catalog::ProgrammingLanguage.all.order(:name)
  end

  # GET /catalog/programming_languages/1
  def show
  end

  # GET /catalog/programming_languages/new
  def new
    @catalog_programming_language = Catalog::ProgrammingLanguage.new
  end

  # GET /catalog/programming_languages/1/edit
  def edit
  end

  # POST /catalog/programming_languages
  def create
    @catalog_programming_language = Catalog::ProgrammingLanguage.new(catalog_programming_language_params)

    if @catalog_programming_language.save
      redirect_to @catalog_programming_language, notice: 'Programming language was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /catalog/programming_languages/1
  def update
    if @catalog_programming_language.update(catalog_programming_language_params)
      redirect_to @catalog_programming_language, notice: 'Programming language was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /catalog/programming_languages/1
  def destroy
    @catalog_programming_language.destroy
    redirect_to catalog_programming_languages_url, notice: 'Programming language was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog_programming_language
      @catalog_programming_language = Catalog::ProgrammingLanguage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_programming_language_params
      params.require(:catalog_programming_language).permit(:name, :url)
    end

    def verify_authentication
      head(403) and return unless current_user.present? && current_user.is_admin?
    end
end
