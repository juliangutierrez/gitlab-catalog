class Catalog::LicensesController < ApplicationController
  before_action :set_catalog_license, only: [:show, :edit, :update, :destroy]
  before_action :verify_authentication

  # GET /catalog/licenses
  def index
    @catalog_licenses = Catalog::License.all.order(:name)
  end

  # GET /catalog/licenses/1
  def show
  end

  # GET /catalog/licenses/new
  def new
    @catalog_license = Catalog::License.new
  end

  # GET /catalog/licenses/1/edit
  def edit
  end

  # POST /catalog/licenses
  def create
    @catalog_license = Catalog::License.new(catalog_license_params)

    if @catalog_license.save
      redirect_to @catalog_license, notice: 'License was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /catalog/licenses/1
  def update
    if @catalog_license.update(catalog_license_params)
      redirect_to @catalog_license, notice: 'License was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /catalog/licenses/1
  def destroy
    @catalog_license.destroy
    redirect_to catalog_licenses_url, notice: 'License was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog_license
      @catalog_license = Catalog::License.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_license_params
      params.require(:catalog_license).permit(:name, :short_name, :url)
    end

    def verify_authentication
      head(403) and return unless current_user.present? && current_user.is_admin?
    end
end
