class Catalog::ProjectsController < ApplicationController
  before_action :set_catalog_project, only: [:new, :show, :edit, :update, :destroy, :notify_interest, :withdraw_from_project]
  before_action :authorize_creation, only: [:create, :new]
  before_action :authorize_edition, only: [:edit, :update, :destroy]
  before_action :authorize_show_interest, only: [:show_interest_in_project]
  before_action :authorize_withdraw_from_project, only: [:withdraw_from_project]

  # GET /catalog/projects
  def index
    @catalog_projects = Catalog::Project.all.order(:name) 
  end

  # GET /catalog/projects/1
  def show
  end

  # GET /catalog/projects/new
  def new
    @catalog_project = catalog_project_template
  end

  # GET /catalog/projects/1/edit
  def edit    
  end

  # POST /catalog/projects
  def create
    @catalog_project = Catalog::Project.new(catalog_project_params)

    if @catalog_project.save
      redirect_to @catalog_project, notice: 'Project was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /catalog/projects/1
  def update
    if @catalog_project.update(catalog_project_params)
      redirect_to @catalog_project, notice: 'Project was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /catalog/projects/1
  def destroy
    @catalog_project.destroy
    redirect_to catalog_projects_url, notice: 'Project was successfully destroyed.'
  end

  def search
    @query = params[:query]
    @catalog_projects = Catalog::Project.search(@query)
    render 'index' 
  end

  def programming_languages
    render json: Catalog::ProgrammingLanguage.search(params[:term])
  end

  def licenses
    render json: Catalog::License.search(params[:term])
  end

  def categories
    render json: Catalog::Category.search(params[:term])
  end

  def countries
    render json: Catalog::Country.search(params[:term])
  end

  def notify_interest
    @catalog_project.interested_countries << current_user.catalog_country
    if @catalog_project.country.user.present?
      NotificationService.new.new_interested_country(@catalog_project, current_user.catalog_country) 
      redirect_to @catalog_project, notice: 'An email will be sent to the projects administrator.'
    else
      redirect_to @catalog_project, alert: "There is no administrator for #{@catalog_project.country.name} projects. No email was sent."
    end
  end

  def withdraw_from_project
    @catalog_project.interested_countries.delete(current_user.catalog_country)
    @catalog_project.save
    redirect_to @catalog_project, notice: 'Your country has been removed from the interest list'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog_project
      @catalog_project = Catalog::Project.find_by_slug(params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_project_params
      params.require(:catalog_project).permit(:name,
                                              :description, 
                                              :slug, 
                                              :url,
                                              :visibility,
                                              :has_country_property,
                                              :operational_systems,
                                              :frameworks,
                                              :dependencies,
                                              :using_since,
                                              :documentation_status,
                                              :documentation_link,
                                              :is_used_by_companies,
                                              :has_services_by_companies,
                                              :plataform,
                                              :developing_country_ids => [], 
                                              :using_country_ids => [], 
                                              :interested_country_ids => [], 
                                              :programming_language_ids => [], 
                                              :license_ids => [], 
                                              :category_ids => [], 
                                              :categories_attributes => [:id,:name])
    end

    def authorize_creation
      head '404' and return unless can?(current_user, :create_projects, catalog_project_template)
    end

    def authorize_edition
      head '404' and return unless can?(current_user, :manage_projects, @catalog_project)
    end

    def authorize_show_interest
      head '404' and return unless can?(current_user, :show_interest_in_project, @catalog_project)
    end

    def authorize_withdraw_from_project
      head '404' and return unless can?(current_user, :withdraw_from_project, @catalog_project)
    end

    def catalog_project_template
      current_user.is_admin?? country = Catalog::Country.find(params[:country_id]) : country = current_user.catalog_country
      Catalog::Project.new developing_countries: [country]
    end
end
