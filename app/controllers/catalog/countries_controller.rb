class Catalog::CountriesController < ApplicationController
  before_action :set_catalog_country, only: [:show, :edit, :update, :destroy]
  before_action :authorize, except: [:index, :show]

  # GET /catalog/countries
  def index
    @catalog_countries = Catalog::Country.all.order(:name)
  end

  # GET /catalog/countries/1
  def show
  end

  # GET /catalog/countries/new
  def new
    @catalog_country = Catalog::Country.new
  end

  # GET /catalog/countries/1/edit
  def edit
  end

  # POST /catalog/countries
  def create
    @catalog_country = Catalog::Country.new(catalog_country_params)

    if @catalog_country.save
      redirect_to @catalog_country, notice: 'Country was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /catalog/countries/1
  def update
    if @catalog_country.update(catalog_country_params)
      redirect_to @catalog_country, notice: 'Country was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /catalog/countries/1
  def destroy
    @catalog_country.destroy
    redirect_to catalog_countries_url, notice: 'Country was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog_country
      @catalog_country = Catalog::Country.find_by_slug(params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_country_params
      params.require(:catalog_country).permit(:name, :slug, :flag, :about, :url)
    end

    def authorize
      head '404' and return unless can?(current_user, :manage_countries, Catalog::Country.new)  
    end
end
