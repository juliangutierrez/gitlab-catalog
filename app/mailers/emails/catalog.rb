module Emails
  module Catalog
    def new_interested_country_email(catalog_project, country)
      @user = catalog_project.country.user
      @catalog_project = catalog_project
      @country = country
      @response_email = country.user.notification_email
      mail(to: @user.notification_email, subject: subject("Country interested in project"))
    end
  end
end