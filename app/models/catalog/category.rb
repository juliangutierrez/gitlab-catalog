class Catalog::Category < ActiveRecord::Base
	has_and_belongs_to_many :projects
	validates :name, presence: true, uniqueness: true

	def self.search(query)
    query = "%#{query}%"
    name_match = arel_table[:name].matches(query)
    where(name_match)
  end
end