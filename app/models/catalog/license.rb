class Catalog::License < ActiveRecord::Base
	validates :name, :short_name, :url, presence: true
	validates :name, uniqueness: true

	def self.search(query)
    query = "%#{query}%"
    name_match = arel_table[:name].matches(query)
    where(name_match)
  end
end
