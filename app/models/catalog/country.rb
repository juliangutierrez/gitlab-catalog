class Catalog::Country < ActiveRecord::Base
	has_and_belongs_to_many :developing_projects, class_name: 'Project', :join_table => :catalog_countries_developing_projects
  has_and_belongs_to_many :interested_projects, class_name: 'Project', :join_table => :catalog_countries_interested_in_projects
  has_and_belongs_to_many :using_projects, class_name: 'Project', :join_table => :catalog_countries_using_projects
  has_one :user, foreign_key: 'catalog_country_id'

	validates :name, :slug, presence: true, uniqueness: true

	def to_param
    slug
  end

  def self.search(query)
    query = "%#{query}%"
    name_match = arel_table[:name].matches(query)
    where(name_match)
  end
end
