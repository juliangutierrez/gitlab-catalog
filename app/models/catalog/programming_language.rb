class Catalog::ProgrammingLanguage < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true

	def self.search(query)
    query = "%#{query}%"
    name_match = arel_table[:name].matches(query)
    where(name_match)
  end
end
