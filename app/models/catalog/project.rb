class Catalog::Project < ActiveRecord::Base
	has_and_belongs_to_many :developing_countries, class_name: 'Country', :join_table => :catalog_countries_developing_projects
  has_and_belongs_to_many :interested_countries, class_name: 'Country', :join_table => :catalog_countries_interested_in_projects
  has_and_belongs_to_many :using_countries, class_name: 'Country', :join_table => :catalog_countries_using_projects

  has_and_belongs_to_many :licenses
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :programming_languages
  
  accepts_nested_attributes_for :categories

	validates :name, :slug, :url, presence: true
  validates :slug, uniqueness: true

  VISIBILITIES = {1 => 'public', 2 => 'private', 3 => 'internal'}
  PLATAFORMS = {1 => 'web', 2 => 'desktop', 3 => 'mobile'}
  DOCUMENTATION_STATUSES = {1 => 'none', 2 => 'partial', 3 => 'sufficient', 4 => 'complete'}

  def to_param
    slug
  end

  def country
    developing_countries.first
  end

  def self.search(query)
    query = "%#{query}%"
    name_match = arel_table[:name].matches(query)
    description_match = arel_table[:description].matches(query)
    where(name_match.or(description_match))
  end

  def self.visibilities
    VISIBILITIES.each_with_object({}) { |(key, value), hash| hash[key] = I18n.t('catalog_project.visibilities.' + value) }
  end

  def self.plataforms
    PLATAFORMS.each_with_object({}) { |(key, value), hash| hash[key] = I18n.t('catalog_project.plataforms.' + value) }
  end

  def self.documentation_statuses
    DOCUMENTATION_STATUSES.each_with_object({}) { |(key, value), hash| hash[key] = I18n.t('catalog_project.documentation_statuses.' + value) }
  end
end
